from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from .models import Question
from .forms import QuestionForm
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

def index(request):
    q = Question.objects.exclude(status=False)
    response = {'question': q}
    return render(request, 'index.html', response)

@login_required
def add_question(request):
    context ={}
    data = {}
    form = QuestionForm(request.POST or None)

    if request.is_ajax(): 
        if (form.is_valid() and request.method=='POST'):
            form.save()
            data['pertanyaan'] = form.cleaned_data.get('pertanyaan')
            return JsonResponse(data)
  
    context['questionform']= form
    return render(request, "faq_form.html", context)

@csrf_exempt
def GetJson(request):
    if(request.method == 'POST'):
        print(request.body)
        data = json.loads(request.body)
        faq = Question(pertanyaan=data['pertanyaan'], jawaban=data['jawaban'], status=data['status'])
        faq.save()
    pertanyaan = Question.objects.exclude(status=False)
    dataPertanyaan = serializers.serialize('json', pertanyaan)
    dataPertanyaan = json.loads(dataPertanyaan)
    return JsonResponse(dataPertanyaan, safe=False)
