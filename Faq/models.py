from django.db import models

class Question(models.Model):
    pertanyaan = models.CharField(max_length=1000)
    jawaban = models.CharField(max_length=1000)
    status = models.BooleanField(default=False)
