from django.urls import path
from .views import index, add_question, GetJson
app_name = 'faq'
urlpatterns = [
   path("", index, name="index"),
   path("addQ", add_question, name="add_question"),
   path("json",GetJson, name="json")
]
