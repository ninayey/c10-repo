const alertBox = document.getElementById('alert-box')
const form = document.getElementById('p-form')
const pertanyaan = document.getElementById('id_pertanyaan')

const csrf = document.getElementsByName('csrfmiddlewaretoken')
console.log(csrf)

const url = ""

const handleAlerts = (type, text) =>{
    alertBox.innerHTML = `<div class="alert alert-${type}" role="alert">
                            ${text}
                        </div>`
}


form.addEventListener('submit', e=>{
    e.preventDefault()

    const fd = new FormData()
    fd.append('csrfmiddlewaretoken', csrf[0].value)
    fd.append('pertanyaan', pertanyaan.value)

    $.ajax({
        type: 'POST',
        url: url,
        enctype: 'multipart/form-data',
        data: fd,
        success: function(response){
            console.log(response)
            const sText = `successfully saved ${response.pertanyaan}`
            handleAlerts('success', sText)
            setTimeout(()=>{
                alertBox.innerHTML = ""
                pertanyaan.value = ""

            }, 3000)
        },
        error: function(error){
            console.log(error)
            handleAlerts('danger', 'ups..something went wrong')
        },
        cache: false,
        contentType: false,
        processData: false,
    })
})
console.log(alertBox)
console.log(pertanyaan)
console.log(form)