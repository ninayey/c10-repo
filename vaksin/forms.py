from django.forms import ModelForm, DateInput, widgets
from .models import Date

class InputDate(DateInput):
    input_type = 'date'

class DateForm(ModelForm):
    class Meta:
        model = Date
        fields = "__all__"
        widgets = {'date':InputDate()}