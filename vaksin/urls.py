from django.urls import path
from .views import index, sukses, GetJson

urlpatterns = [
    path('', index, name='index'),
    path('sukses',sukses,name="sukses"),
    path("json",GetJson, name="json"),
]