from django.http import response
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from .forms import SignUpForm, LoginForm
from .models import Pengguna
from django.contrib import messages

# Create your views here.
def index(request):
    user = request.user
    if user.is_authenticated:
        return render(request, 'pengguna_index.html')

def log_in (request):
    response = {}
    user = request.user
    if user.is_authenticated: 
        return redirect("/")
        
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            email = request.POST['email']
            password = request.POST['password']
            user = authenticate(email=email, password=password)
            
            if user:
                login(request, user)
                messages.success(request, "Hi, " + user.username + "!")
                return redirect("/")
    else:
        form = LoginForm()
        
    response['login_form'] = form
    return render(request, "pengguna_login.html", response)

def sign_up (request):
    response = {}
    if request.POST:
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            account = authenticate(email=email, password=raw_password)
            login(request, account)
            return redirect('/')
        else:
            response['signup_form'] = form
            
    else:
        form = SignUpForm()
        response['signup_form'] = form
    return render(request, 'pengguna_signup.html', response)

def logoutUser(request):
    logout(request)
    messages.success(request, "Logout berhasil!")
    return redirect('/')
