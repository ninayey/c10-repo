from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from .models import Pengguna


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Email harus diisi.')

    class Meta:
        model = Pengguna
        fields = ('email', 'username', 'password1', 'password2', )


class LoginForm(forms.ModelForm):

	password = forms.CharField(label='Password', widget=forms.PasswordInput)

	class Meta:
		model = Pengguna
		fields = ('email', 'password')

	def clean(self):
		if self.is_valid():
			email = self.cleaned_data['email']
			password = self.cleaned_data['password']
			if not authenticate(email=email, password=password):
				raise forms.ValidationError("Login tidak berhasil")

