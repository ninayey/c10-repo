from django.urls import path
from .views import index, log_in, sign_up, logoutUser

urlpatterns = [
    path('', index, name='index'),
    path('login', log_in, name = 'login'),
    path('signup', sign_up, name = 'signup'),
    path('logout', logoutUser, name = 'logout')
]