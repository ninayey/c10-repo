from typing import Text
from django import forms
from django.forms.widgets import Textarea
from .models import Message


class FormMessage(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['tittle', 'message']

        error_messages = {
            'required' : 'Masukkan Pesan'
        }

        input_attrs = {
            'type' : 'text',
        }

        title = forms.CharField(label="Title", required=True, widget=forms.TextInput(attrs=input_attrs), error_messages=error_messages)
        message = forms.CharField(label="Message", required=True, widget=forms.Textarea(attrs=input_attrs), error_messages=error_messages)