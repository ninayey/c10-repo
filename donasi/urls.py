from django.urls import path
from donasi.views import get_all_message, konfirmasi
from .views import index, donasi_umum, donasi_apd, donasi_oksigen

urlpatterns = [
    path('', index, name='index'),
    path('umum', donasi_umum, name='umum'),
    path('apd', donasi_apd, name='apd'),
    path('tabung-oksigen', donasi_oksigen, name='tabung-oksigen'),
    path('all-message', get_all_message),
    path('konfirmasi', konfirmasi)
]