from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from donasi.models import Message
from .forms import FormMessage
from django.core import serializers

# Create your views here.
def index(request):
    all_message = Message.objects.all()
    form = FormMessage()

    if request.is_ajax():
        form  = FormMessage(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()
            return HttpResponseRedirect('/donasi')
    response = {'all_message': all_message, 'form': form}
    return render(request, 'donasi_index.html', response)


def donasi_umum(request):
    return render(request, 'donasi_umum.html')


def donasi_apd(request):
    return render(request, 'donasi_apd.html')


def donasi_oksigen(request):
    return render(request, 'donasi_oksigen.html')


def konfirmasi(request):
    return render(request, 'konfirmasi.html')


def get_all_message(request):
    donasi = Message.objects.all()
    data = serializers.serialize('json', donasi)
    return HttpResponse(data, content_type="application/json")