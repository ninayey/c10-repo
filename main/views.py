from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from .forms import FeedbackForm
from .models import Feedback
import json

def feedback(request):
    response = {}
    if request.method == 'POST':
        if request.is_ajax():
            FB_form = FeedbackForm(request.POST)
            if FB_form.is_valid():
                FB = Feedback()
                FB.name = request.user.username
                FB.message = FB_form.cleaned_data['message']
                FB.save()
                return JsonResponse({'success':True})
        return JsonResponse({'success':False})
    else:
        form = FeedbackForm()
        response = {'form':form}
        response['data'] = Feedback.objects.all()
        return render(request, 'main/feedback.html',response)

def home(request):
    form = FeedbackForm()
    response = {'form':form}
    return render(request, 'main/home.html',response)
    
    
def about_us(request):
    return render(request, 'main/about_us.html')

def get_feedback(request):
    userReq = Feedback.objects.all().values()[::-1]
    lst = list(userReq)
    #print(lst)
    return JsonResponse(lst, safe=False)
