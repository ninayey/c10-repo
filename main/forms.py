from django import forms

class FeedbackForm(forms.Form):

    message = forms.CharField(label="Feedback Form", widget=forms.Textarea(attrs={
        'rows': 6,
        'style': 'resize:none;',
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Tulis Feedback-mu di sini ya!',
        'required': True,
        'id':'message_input'
    }))
