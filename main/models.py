from django.db import models

# Create your models here.
class Feedback(models.Model):
    name = models.CharField(max_length=50, blank=True)
    message = models.CharField(max_length=300, blank=False)

    def get_dict(self):
        return {
            'name':self.name,
            'message':self.message
        }
    
    def __str__(self):
        return self.name + " - " + self.message

