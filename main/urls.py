from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('about-us/', views.about_us, name='about_us'),
    path('feedback/', views.feedback, name='feedback'),
    path('get-feedback/', views.get_feedback, name='get_feedback')

]
