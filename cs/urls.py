from django.contrib import admin
from django.urls import path
from .views import index, tambah, tambahFlutter

admin.autodiscover

urlpatterns = [
    path('', index, name='index'),
    path('tambah/', tambah, name='tambah'),
    path('tambahFlutter/', tambahFlutter, name='tambahFlutter'),
]

"""
urlpatterns = [
    re_path(r'^$', 'cs.views.index'),
    re_path(r'^tambah/$', 'cs.views.tambah'),

    #url(r'', index, name='index'),
    #url(r'^tambah/$', tambah, name='tambah'),
    #url(r'^admin/', include(admin.site.urls)),
]
"""