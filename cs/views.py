from .models import Media
from django.shortcuts import render
from django.http.response import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
#from django.contrib.auth.decorators import login_required
#@login_required(login_url='/admin/login/')

def index(request):
    return render(request, 'cs_index.html')

def tambah(request):
    if request.method == 'POST':
        nama = request.POST['nama']
        media = request.POST['media']

    Media.objects.create(
        nama = nama,
        media = media
    )
    return HttpResponse('')

@csrf_exempt
def tambahFlutter(request):
    nama = ''
    media = ''

    if request.method == 'POST':
        nama = request.POST['nama']
        media = request.POST['media']

    Media.objects.create(
        nama = nama,
        media = media
    )
    return JsonResponse({'nama': nama, 'media': media})
